const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const AlbumRoute = require('./src/routes/AlbumRoute');
const AuthRoute = require('./src/routes/AuthRoute');require('dotenv').config();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + "/src/public"));

app.set('view engine', 'ejs');
app.set('views', './src/views');
app.use('/', AuthRoute);
app.use('/myalbums', AlbumRoute);
app.get("*", (req, res) => {
  res.status(404).send("Not found !!!");
});

app.listen(process.env.PORT, () => {
  console.log(`localhost:${process.env.PORT} running...`);
});
