const gulp = require("gulp"),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    del = require('del'),
    concat = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    cssNano = require('gulp-cssnano'),
    minify = require("gulp-minify");

gulp.task('scripts', () => {
    return gulp.src('src/public/js/*.js')
       .pipe(uglify())
       .pipe(minify())
       .pipe(concat('main.min.js'))
       .pipe(gulp.dest('src/public/js'))
});

gulp.task('styles', () => {
    return gulp.src('src/public/css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concatCss('main.min.css'))
        .pipe(cssNano())
        .pipe(gulp.dest('src/public/css'));
});


gulp.task('clean', () => {
    return del([
        'css/main.css',
    ]);
});

gulp.task('default', gulp.series(['clean', 'styles', 'scripts']));

gulp.task('watch', () => {
    gulp.watch('src/public/css/*.scss', (done) => {
        gulp.series('sass', 'minify');
        gulp.series(['clean', 'styles'])(done);
    });
});
