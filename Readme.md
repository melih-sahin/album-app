# Album Application


User login info:

email: `user@test.com`

password: `123456`


### Installation (Manual)

```
$ git clone git@bitbucket.org:melih-sahin/album-app.git
$ bash run.sh
```
Open browser `localhost:3000`

### Demo
[https://album-web-app.herokuapp.com](https://album-web-app.herokuapp.com)
