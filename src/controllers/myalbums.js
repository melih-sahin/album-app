const request = require('../utils/request');

class MyAlbumsController {
    async Home(req, res) {
        try {
            const response = await request.get('/albums');
            let albums = response.data.slice(0, 10);
            res.render('my_albums', {albums});
        } catch (e) {
            res.send(e);
        }
    };

    async Show(req, res) {
        try {
            const response = await request.get(`/albums/${req.params.id}/photos`);
            let photos = response.data.slice(0, 8);
            res.send(photos);
        } catch (e) {
            res.send(e);
        }
    }
}

module.exports = new MyAlbumsController();