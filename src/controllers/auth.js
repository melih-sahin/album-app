const {validationResult} = require('express-validator');
const formData = require("express-form-data");
const express = require('express');
const app = express();

app.use(formData.parse());
require('dotenv').config();

class AuthController {
    Index(req, res) {
        res.render('index');
    }

    Login(req, res) {
        const mockData = {
            'email': process.env.AUTH_USER_EMAIL,
            'password': process.env.AUTH_USER_PASSWORD
        }
        let email = req.body.email;
        let password = req.body.password;

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const alert = errors.array();
            res.render('index', {alert});
        }

        if (email === mockData.email && password === mockData.password) {
            return res.redirect('/myalbums');
            
        }
    }
}

module.exports =  new AuthController();