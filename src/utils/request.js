const axios = require('axios');
require('dotenv').config();

let axiosConfig = {
    baseURL: process.env.ALBUM_DATA_URL,
    timeout: 30000
};
const Request = axios.create(axiosConfig);

module.exports = Request;