const express = require('express');
const AlbumRoute = express.Router();
const MyAlbumsController = require('../controllers/myalbums')

AlbumRoute.get('/', MyAlbumsController.Home);
AlbumRoute.get('/:id', MyAlbumsController.Show);

module.exports = AlbumRoute;