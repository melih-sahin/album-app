const express = require('express');
const AuthRoute = express.Router();
const AuthController = require('../controllers/auth')
const {check} = require('express-validator');

AuthRoute.get('/', AuthController.Index);

AuthRoute.post(
    '/',
    check('email', 'Invalid e-mail format or does not exists')
    .exists()
    .isEmail(),
    check('password', 'The password must not be shorter than 5 characters!')
    .isLength({ min: 5 }),
    AuthController.Login
);
module.exports = AuthRoute;