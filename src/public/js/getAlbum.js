var maxWidth = window.matchMedia("(max-width: 700px)");

function getAlbum (id) {
  removeOldData();

  var req = new XMLHttpRequest();
  req.onreadystatechange = processResponse;
  req.open("GET", `myalbums/${id}`);
  req.send();

  function processResponse() {
    if (req.readyState === XMLHttpRequest.DONE) {
      if (req.status === 200) {
        let resData = JSON.parse(req.responseText);

        resData.forEach((data) => {
          const el = document.getElementById('photo-row');
          
          el.innerHTML += `<div id="photo-box" class="col-3">
          <img src="${data.thumbnailUrl}" class="pointer" alt="${data.title}" onclick=getBigImage('${data.url}')>
          </div>`;
        });

      } else {
        console.log('request error');
      }
    }
  }
}

function removeOldData (){
 const elPhoto = document.getElementById('photo-row');
 const elBigPhoto = document.getElementById('big-photo');
 const photos = document.getElementById('photos');

  while (elPhoto.firstChild) {
    elPhoto.removeChild(elPhoto.firstChild);
  }
  
  elBigPhoto.innerHTML = "";

  if(maxWidth.matches) {
    photos.style.marginBottom = "27em";
    elPhoto.style.height = "52em";
  }else {
    photos.style.marginBottom = "100px";
  }
}

function getBigImage (url) {
  const el = document.getElementById('big-photo');
  resizeDiv(el)

  el.innerHTML = `<img src="${url}" class="responsive-image" alt="image">`
}

function resizeDiv(el) {
  el.style.marginTop = maxWidth.matches ? "auto" : "10px";
  el.style.height ="auto";
  maxWidth.addListener(resizeDiv);
}
